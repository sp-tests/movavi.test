<?php

namespace app\commands;

use app\common\currency\CbrRateResource;
use app\common\currency\RbcRateResource;
use app\common\http\GuzzleClientAdapter;
use app\lib\currency\dto\Currency;
use app\lib\currency\dto\CurrencyRateRequest;
use app\lib\currency\rate\AvgCompositeResource;
use app\lib\currency\rate\ResourceInterface;
use yii\console\Controller;

/**
 * Class CurrencyRateController
 * @package app\commands
 */
class CurrencyRateController extends Controller
{
    /**
     * @return ResourceInterface
     */
    protected function getRateResource()
    {
        $httpClient = new GuzzleClientAdapter();

//        return (new RbcRateResource($httpClient))->setSource(RbcRateResource::SOURCE_FOREX);
//        return new CbrRateResource($httpClient);

        return new AvgCompositeResource(
            new RbcRateResource($httpClient),
            new CbrRateResource($httpClient)
        );
    }

    /**
     * Можно вынести в какую-нибудь фабрику CurrencyFactory::createByIso(string $isoCode)
     *
     * @param string $isoCode
     * @return Currency
     */
    protected function isoCurrency(string $isoCode): Currency
    {
        return (new Currency())->setIsoCode($isoCode);
    }

    /**
     * @param string $date
     * @throws \Exception
     */
    public function actionRurUsd($date = 'now')
    {
        $date = new \DateTime($date); //todo must be foolproof?
        $rateRequest = new CurrencyRateRequest(
            $this->isoCurrency(Currency::ISO_CODE_USD),
            $this->isoCurrency(Currency::ISO_CODE_RUB),
            $date
        );

        try {
            $rate =  $this->getRateResource()
                ->getCurrencyRate($rateRequest)
                ->getRate();

            echo "Currency Rate: " . $rate . PHP_EOL;
        } catch (\Exception $e) {
            throw new \Exception('Can\'t get currency rate. Reason: ' . $e->getMessage());
        }
    }


}