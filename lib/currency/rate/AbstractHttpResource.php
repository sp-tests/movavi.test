<?php

namespace app\lib\currency\rate;

use app\lib\currency\dto\CurrencyRate;
use app\lib\currency\dto\CurrencyRateRequest;
use app\lib\http\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class AbstractHttpResource
 * @package app\lib\currency\dto
 */
abstract class AbstractHttpResource implements ResourceInterface
{
    /**
     * @inheritdoc
     */
    final public function getCurrencyRate(CurrencyRateRequest $request): CurrencyRate
    {
        $httpRequest = $this->buildHttpRequest($request);
        $response = $this->getClient()->sendRequest($httpRequest);

        return $this->getRateFromResponse($response, $request);
    }

    /**
     * Http-клиент, используемый для отправки запросов на ресурс
     *
     * @return ClientInterface
     */
    abstract protected function getClient(): ClientInterface;

    /**
     * Построение запроса для получения курса валют по параметрам
     *
     * @param CurrencyRateRequest $request
     * @return RequestInterface
     */
    abstract protected function buildHttpRequest(CurrencyRateRequest $request): RequestInterface;

    /**
     * Получение значения курса из ответа сервера
     *
     * @param ResponseInterface $response
     * @param CurrencyRateRequest $request
     * @return CurrencyRate
     * @throws \Exception
     */
    abstract protected function getRateFromResponse(ResponseInterface $response, CurrencyRateRequest $request): CurrencyRate;
}