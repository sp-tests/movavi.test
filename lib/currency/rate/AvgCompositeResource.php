<?php

namespace app\lib\currency\rate;
use app\lib\currency\dto\CurrencyRate;
use app\lib\currency\dto\CurrencyRateRequest;

/**
 * В принципе можно сделать отдельно CompositeResource и описать поведение для работы с множеством ресурсов.
 * Напр-р
 *      public addResource(ResourceInterface $resource)
 *      public use($resourceName)
 *      protected getRateFromAllResources(CurrencyRateRequest $request)
 *      и т.п.
 * И от него уже отнаследовать AvgCompositeResource.
 *
 * Class AvgCompositeResource
 * @package app\lib\currency\dto
 */
class AvgCompositeResource implements ResourceInterface
{
    /**
     * @var ResourceInterface[]
     */
    protected $resources = [];

    /**
     * AvgCompositeResource constructor.
     * @param ResourceInterface[] ...$resources
     */
    public function __construct(ResourceInterface ...$resources)
    {
        $this->resources = $resources;
    }

    /**
     * @inheritdoc
     */
    public function getCurrencyRate(CurrencyRateRequest $request): CurrencyRate
    {
        $currencyRate = new CurrencyRate(
            $request->getSourceCurrency(),
            $request->getTargetCurrency()
        );
        $result = [];

        foreach ($this->resources as $resource) {
            $result[] = $resource->getCurrencyRate($request)->getRate();
        }

        $avgRate = array_sum($result) / count($result);

        return $currencyRate->setRate($avgRate);
    }

}