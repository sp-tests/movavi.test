<?php

namespace app\lib\currency\dto;

/**
 * Class Currency
 * @package app\lib\currency\dto
 */
class Currency
{
    const ISO_CODE_RUB = 'RUR';
    const ISO_CODE_USD = 'USD';

    /**
     * @var string
     */
    protected $isoCode = '';

    /**
     * @param string $isoCode
     * @return Currency
     */
    public function setIsoCode(string $isoCode): Currency
    {
        $this->isoCode = $isoCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsoCode(): string
    {
        return $this->isoCode;
    }
}