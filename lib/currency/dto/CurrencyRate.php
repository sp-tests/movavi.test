<?php

namespace app\lib\currency\dto;

/**
 * Class CurrencyRate
 * @package app\lib\currency\dto
 */
class CurrencyRate
{
    /**
     * @var Currency
     */
    protected $sourceCurrency;

    /**
     * @var Currency
     */
    protected $targetCurrency;

    /**
     * @var float
     */
    protected $rate = 0;

    /**
     * CurrencyRate constructor.
     * @param Currency $source
     * @param Currency $target
     */
    public function __construct(Currency $source, Currency $target)
    {
        $this->sourceCurrency = $source;
        $this->targetCurrency = $target;
    }

    /**
     * @param float $rate
     * @return CurrencyRate
     */
    public function setRate(float $rate): CurrencyRate
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return Currency
     */
    public function getSourceCurrency(): Currency
    {
        return $this->sourceCurrency;
    }

    /**
     * @return Currency
     */
    public function getTargetCurrency(): Currency
    {
        return $this->targetCurrency;
    }
}