<?php

namespace app\common\currency;

use app\lib\currency\rate\AbstractHttpResource;
use app\lib\http\ClientInterface;

/**
 * Общий класс для ресурсов, позволяющих задать http-клиента извне.
 * Почему-то не захотел выносить это поведение в общий абстрактный класс.
 *
 * Class SharedClientHttpResource
 * @package app\common\currency
 */
abstract class SharedClientHttpResource extends AbstractHttpResource
{
    /**
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * SharedClientHttpResource constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->httpClient = $client;
    }

    /**
     * @return ClientInterface
     */
    protected function getClient(): ClientInterface
    {
        return $this->httpClient;
    }
}