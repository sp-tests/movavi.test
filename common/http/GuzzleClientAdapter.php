<?php

namespace app\common\http;
use app\lib\http\ClientInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class GuzzleClientAdapter
 * @package app\common\http
 */
class GuzzleClientAdapter extends Client implements ClientInterface
{
    /**
     * @inheritdoc
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        return $this->send($request);
    }
}