<?php

use app\common\currency\CbrRateResource;
use app\common\currency\RbcRateResource;
use app\lib\currency\dto\Currency;
use app\lib\currency\dto\CurrencyRateRequest;
use app\tests\lib\http\MockHttpClient;

/**
 * Class RbcRateResourceTest
 */
class RbcRateResourceTest extends \yii\codeception\TestCase
{
    /**
     * @covers RbcRateResource::getCurrencyRate()
     */
    public function testParseValidResourceData()
    {
        $rateRequest = new CurrencyRateRequest(
            (new Currency())->setIsoCode(Currency::ISO_CODE_USD),
            (new Currency())->setIsoCode(Currency::ISO_CODE_RUB)
        );
        $resourceRate = $this->getValidResource()
            ->getCurrencyRate($rateRequest)
            ->getRate();

        $this->assertEquals(63.4549, $resourceRate);
    }

    /**
     * @expectedException \Exception
     */
    public function testInvalidResourceData()
    {
        $rateRequest = new CurrencyRateRequest(
            (new Currency())->setIsoCode(Currency::ISO_CODE_USD),
            (new Currency())->setIsoCode(Currency::ISO_CODE_RUB)
        );
        $resource = new CbrRateResource(new MockHttpClient(''));
        $resource->getCurrencyRate($rateRequest);
    }

    /**
     * @return RbcRateResource
     */
    protected function getValidResource(): RbcRateResource
    {
        return new RbcRateResource(
            new MockHttpClient($this->getMockData())
        );
    }

    /**
     * @return string
     */
    protected function getMockData()
    {
        return file_get_contents(__DIR__ . '/../data/rbc_response.json');
    }
}