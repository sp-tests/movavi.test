<?php

use app\common\currency\CbrRateResource;
use app\lib\currency\dto\Currency;
use app\lib\currency\dto\CurrencyRateRequest;
use app\tests\lib\http\MockHttpClient;

/**
 * Class CbrRateResourceTest
 */
class CbrRateResourceTest extends \yii\codeception\TestCase
{
    /**
     * @covers CbrRateResource::ensureRequestIsValid()
     * @expectedException \InvalidArgumentException
     */
    public function testInvalidTargetCurrency()
    {
        $rateRequest = new CurrencyRateRequest(
            new Currency(),
            new Currency()
        );

        $this->getValidResource()->getCurrencyRate($rateRequest);
    }

    /**
     * @covers CbrRateResource::getCurrencyRate()
     */
    public function testParseValidResourceData()
    {
        $rateRequest = new CurrencyRateRequest(
            (new Currency())->setIsoCode(Currency::ISO_CODE_USD),
            (new Currency())->setIsoCode(Currency::ISO_CODE_RUB)
        );
        $resourceRate = $this->getValidResource()
            ->getCurrencyRate($rateRequest)
            ->getRate();

        $this->assertEquals(30.9436, $resourceRate);
    }

    /**
     * @expectedException \Exception
     */
    public function testInvalidResourceData()
    {
        $rateRequest = new CurrencyRateRequest(
            (new Currency())->setIsoCode(Currency::ISO_CODE_USD),
            (new Currency())->setIsoCode(Currency::ISO_CODE_RUB)
        );
        $resource = new CbrRateResource(new MockHttpClient(''));
        $resource->getCurrencyRate($rateRequest);
    }

    /**
     * @return CbrRateResource
     */
    protected function getValidResource(): CbrRateResource
    {
        return new CbrRateResource(
            new MockHttpClient($this->getMockData())
        );
    }

    /**
     * @return string
     */
    protected function getMockData()
    {
        return file_get_contents(__DIR__ . '/../data/cbr_response.xml');
    }
}