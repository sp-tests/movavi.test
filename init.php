<?php

require_once(__DIR__ . '/vendor/autoload.php');

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
//$dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD']);

define('YII_DEBUG', getenv('YII_DEBUG') ?: true); 
define('YII_ENV', getenv('YII_ENV') ?: 'dev');

require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');
