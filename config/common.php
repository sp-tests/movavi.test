<?php
$config = [
    'id' => 'movavi-test',
    'basePath' => __DIR__ . '/../',
    'components' => [
//        'db' => require "db.php"
    ],
    'aliases' => [
        '@app' => __DIR__ . '/../',
        '@bower' => '@vendor/bower-asset',
    ],
];

return $config;